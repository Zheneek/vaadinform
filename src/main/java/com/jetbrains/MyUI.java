package com.jetbrains;

import javax.servlet.annotation.WebServlet;

import com.jetbrains.customer.CustomerForm;
import com.jetbrains.model.User;
import com.jetbrains.sql.WorkSql;
import com.jetbrains.storage.CustomerService;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.SelectionEvent;
import com.vaadin.server.ErrorEvent;
import com.vaadin.server.ErrorHandler;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    private CustomerService service = CustomerService.getInstance();

    private static Grid grid = new Grid();
    private TextField filter = new TextField();
    private CustomerForm form = new CustomerForm(this);

    public MyUI() throws SQLException {
    }

    public static Grid getGrid() {
        return grid;
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        final VerticalLayout layout = new VerticalLayout();

        filter.setInputPrompt("Filter by Name...");
        filter.addTextChangeListener(e -> {
            grid.setContainerDataSource(new BeanItemContainer<>(User.class, service.findAll(e.getText())));
        });

        Button addButton = new Button("Add user");
        addButton.addClickListener(e -> {
            grid.select(null);
            form.setUser(new User());
        });



        HorizontalLayout bar = new HorizontalLayout(filter, addButton);
        bar.setSpacing(true);
        grid.setSizeFull();

        grid.setColumns("id", "firstName", "lastName", "other");

        layout.addComponents(bar, grid, form);
        layout.setExpandRatio(grid,1);

        updateList();

        layout.setMargin(true);
        layout.setSpacing(true);
        setContent(layout);

        form.setVisible(false);

        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.addSelectionListener(new SelectionEvent.SelectionListener() {
            @Override
            public void select(SelectionEvent selectionEvent) {

                if(selectionEvent.getSelected().isEmpty()) {
                    form.setVisible(false);
                } else {
                    BeanItem<User> item = new BeanItemContainer<>(User.class, WorkSql.getAll()).getItem(grid.getSelectedRow());
                    form.setUser(item.getBean());
                }
            }
        });

        grid.setEditorErrorHandler(new Grid.EditorErrorHandler() {
            @Override
            public void commitError(Grid.CommitErrorEvent commitErrorEvent) {
                Notification.show(commitErrorEvent.getCause()
                .getInvalidFields().values().iterator().next()
                .getMessage(),
                        Notification.Type.ERROR_MESSAGE);
            }
        });

        addButton.setErrorHandler(new ErrorHandler() {
            @Override
            public void error(com.vaadin.server.ErrorEvent errorEvent) {
                Notification.show(errorEvent.getThrowable().getLocalizedMessage());
            }
        });
    }

    public void updateList() {

        List<User> users = WorkSql.getAll();
        grid.setContainerDataSource(new BeanItemContainer<>(User.class, users));
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {

    }
}