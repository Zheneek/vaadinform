package com.jetbrains.sql;

import com.jetbrains.model.User;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.*;

/**
 * Created by admin on 16.05.2018.
 */
public class WorkSql {

    private static Connection connection;

    static {

        String url = null;
        String name = null;
        String password = null;

        try(InputStream in = WorkSql.class.getClassLoader().getResourceAsStream("persistence.properties")) {

            Properties properties = new Properties();
            properties.load(in);
            url = properties.getProperty("url");
            name = properties.getProperty("username");
            password = properties.getProperty("password");
        } catch (IOException e) {

        }

        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url,name,password);
        } catch (SQLException | ClassNotFoundException e) {
        }
    }
    public static List<User> getAll() {

        List<User> users = new ArrayList<>();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM usersnew");
            ResultSet set = ps.executeQuery();

            while (set.next()) {

                User user = new User();
                user.setId(set.getInt(1));
                user.setFirstName(set.getString(2));
                user.setLastName(set.getString(3));
                user.setOther(set.getString(4));
                users.add(user);
            }

            set.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Collections.sort(users, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return (int) (o1.getId() - o2.getId());
            }
        });

        return users;
    }

    public static void synchronizedDeleteDB(User user) {

        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("DELETE FROM usersnew WHERE firtname=?");
            ps.setString(1,user.getFirstName());

            ps.executeUpdate();

            ps.close();

        }  catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public static void synchronizedSaveDB(User user) {

        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("UPDATE usersnew SET firtname=?, lastname=?, other=? WHERE id=?");
            ps.setString(1,user.getFirstName());
            ps.setString(2,user.getLastName());
            ps.setString(3,user.getOther());
            ps.setInt(4,user.getId());

            ps.executeUpdate();

            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void synchronizedAddDB(User user) {

        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("INSERT INTO usersnew (id,firtname,lastname,other) VALUES(?,?,?,?)");
            ps.setInt(1, user.getId());
            ps.setString(2,user.getFirstName());
            ps.setString(3,user.getLastName());
            ps.setString(4,user.getOther());

            ps.executeUpdate();
            ps.close();
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
