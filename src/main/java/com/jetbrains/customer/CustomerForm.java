package com.jetbrains.customer;

import com.jetbrains.MyUI;
import com.jetbrains.model.User;
import com.jetbrains.storage.CustomerService;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import java.sql.SQLException;

/**
 * Created by admin on 16.05.2018.
 */
public class CustomerForm extends FormLayout {

    private TextField id = new TextField("Id");
    private TextField firstName = new TextField("First name");
    private TextField lastName = new TextField("Last name");
    private TextField other = new TextField("Position");
    private Button save = new Button("Save");
    private Button delete = new Button("Delete");
    private Button add = new Button("Add");
    private Button cancel = new Button("Cancel");

    private CustomerService service = CustomerService.getInstance();
    private User user;
    private MyUI myUI;

    public CustomerForm(MyUI myUI) throws SQLException {
        this.myUI = myUI;

        save.setStyleName(ValoTheme.BUTTON_PRIMARY);
        save.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        add.setStyleName(ValoTheme.BUTTON_PRIMARY);

        save.addClickListener(e -> save());
        delete.addClickListener(e -> delete());
        add.addClickListener(e -> add());
        cancel.addClickListener(e -> cancel());

        setSizeUndefined();
        HorizontalLayout form = new HorizontalLayout(id, firstName, lastName, other);
        HorizontalLayout buttons = new HorizontalLayout(save, delete, add, cancel);;
        buttons.setSpacing(true);
        form.setSpacing(true);
        addComponents(form, buttons);
    }

    public void setUser(User user) {
        this.user = user;
        BeanFieldGroup.bindFieldsUnbuffered(user,this);

        save.setVisible(user.isEmpty());
        delete.setVisible(user.isEmpty());
        add.setVisible(!user.isEmpty());
        id.setEnabled(!user.isEmpty());
        setVisible(true);
        firstName.selectAll();
    }

    public void delete() {
        service.delete(user);
        myUI.updateList();
    }

    public void save() {
        service.save(user);
        myUI.updateList();
    }

    public void add() {
        service.add(user);
        myUI.updateList();
    }

    public void cancel() {
        myUI.updateList();
        setVisible(false);
    }
}
