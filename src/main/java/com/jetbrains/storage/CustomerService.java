package com.jetbrains.storage;

import com.jetbrains.model.User;
import com.jetbrains.sql.WorkSql;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 16.05.2018.
 */
public class CustomerService {

    private static CustomerService instance;

    private final List<User> users = WorkSql.getAll();

    public List<User> getUsers() {
        return users;
    }

    public static CustomerService getInstance() throws SQLException {
        if (instance == null) {
            instance = new CustomerService();
        }
        return instance;
    }

    public synchronized List<User> findAll(String stringFilter) {
        ArrayList<User> arrayList = new ArrayList<>();
        for (User contact : users) {
            boolean passesFilter = (stringFilter == null || stringFilter.isEmpty())
                    || contact.toString().toLowerCase().contains(stringFilter.toLowerCase());
            if (passesFilter) {
                arrayList.add(contact);
            }
        }

        return arrayList;
    }

    public synchronized void delete(User value) {
        WorkSql.synchronizedDeleteDB(value);
    }

    public void save(User value) {
        WorkSql.synchronizedSaveDB(value);
    }

    public void add(User value) {  WorkSql.synchronizedAddDB(value);}
}
